#include <stdio.h>

int main () {
	float base, iva, irpf;
	float totalFactura;
	int option;

	do {
		printf ("Menu:\n");
		printf ("1- Base imponible\n");
		printf ("2- Total factura\n");
		printf ("3- Exit\n");
		scanf ("%d", &option);

		switch (option) {
			case 1:
				printf ("Base imponible? \n");
				scanf ("%f", &base);
				iva = base * 0.21;
				irpf = base * 0.07;
				totalFactura = base + iva - irpf;
				printf ("Base: %.2f\nIVA: %.2f\nIRPF: %.2f\n", base, iva, irpf);
				printf ("Total factura: %.2f\n", totalFactura);
				break;
			case 2:
				printf ("Total factura?\n");
				scanf ("%f", &totalFactura);
				base = totalFactura / 1.14;
				iva = base * 0.21;
				irpf = base * 0.07;
				printf ("Base: %.2f\nIVA: %.2f\nIRPF: %.2f\n", base, iva, irpf);
				printf ("Total factura: %.2f\n", totalFactura);
				break;
			default:
				printf ("Wrong option.\n");
				break;
		}
	} while (option != 3);
	return 0;
}